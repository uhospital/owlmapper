package br.unisinos.uhospital.owlmapper;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.jena.ext.com.google.common.collect.Lists;
import org.apache.jena.ontology.Individual;
import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntModelSpec;
import org.apache.jena.ontology.OntProperty;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.vocabulary.RDF;
import org.objenesis.Objenesis;
import org.objenesis.ObjenesisStd;
import org.objenesis.instantiator.ObjectInstantiator;

import br.unisinos.uhospital.owlmapper.annotations.OWLClass;
import br.unisinos.uhospital.owlmapper.annotations.OWLIdentifiable;
import br.unisinos.uhospital.owlmapper.annotations.OWLProperty;

public class OWLMapper {
	private OntModel base;
	private Map<Class<?>, Class<?>> mappings;
	
	public OWLMapper() {
		this(null);
	}
	
	public OWLMapper(OntModel base) {
		this.base = base;
		this.mappings = new HashMap<Class<?>, Class<?>>();
	}
	
	public void addMapping(Class<?> from, Class<?> to) {
		mappings.put(from, to);
	}

	public OntModel writeValue(Object obj) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		OntModel model = ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM);
		createIndividual(obj, model);
		return model;
	}
	
	public Object readValue(List<Statement> statements, Class<?> cls) throws InstantiationException, IllegalAccessException, NoSuchMethodException, SecurityException, IllegalArgumentException, InvocationTargetException {
		Method constructor = getConstructorMethod(cls);
		Object anInstance = null;
		if (constructor != null) {
			Parameter[] parameters = constructor.getParameters();
			List<Object> values = new ArrayList<>(); 
			for (Parameter parameter : parameters) {
				OWLProperty propertyAnno = parameter.getAnnotation(OWLProperty.class);
				if (propertyAnno == null)
					continue;
				OntProperty owlProperty = base.getOntProperty(base.expandPrefix(propertyAnno.value()));
				Statement stmt = getStatementWithProperty(owlProperty, statements);
				if (stmt != null) {
					Class<?> type = parameter.getType();
					values.add(castValue(stmt.getObject(), type));
				}
			}
			Class<?>[] classes = values.stream().map(Object::getClass).toArray(size -> new Class<?>[size]);
			Constructor<?> theConstructor = cls.getConstructor(classes);
			return theConstructor.newInstance(values.toArray());
		} else {
			Objenesis objenesis = new ObjenesisStd();
			ObjectInstantiator<?> objInstantiator = objenesis.getInstantiatorOf(cls);
			anInstance = objInstantiator.newInstance();
		}
		List<Method> setters = getSetters(cls);
		for (Method setter : setters) {
			if (isIgnored(setter))
				continue;
			OWLProperty propertyAnno = getPropertyAnnotation(setter);
			if (propertyAnno == null)
				continue;
			OntProperty owlProperty = base.getOntProperty(base.expandPrefix(propertyAnno.value()));
			Statement stmt = getStatementWithProperty(owlProperty, statements);
			if (stmt != null) {
				Class<?> type = setter.getParameterTypes()[0];
				setter.invoke(anInstance, castValue(stmt.getObject(), type));
			}
		}
		return anInstance;
	}

	private Method getConstructorMethod(Class<?> cls) {
		Method constructor = searchMethod("constructor", cls);
		if (constructor != null)
			return constructor;
		Class<?> mixin = mappings.get(cls);
		if (mixin != null) {
			return searchMethod("constructor", mixin);
		}
		return null;
	}
	
	private List<Method> getGetters(Class<?> cls) {
		List<Method> methods = new ArrayList<>();
		while (cls != null) {
			for (Method method : cls.getDeclaredMethods()) {
				if (method.getName().startsWith("get") && method.getParameterCount() == 0)
					methods.add(method);
			}
			cls = cls.getSuperclass();
		}
		return methods;
	}
	
	private List<Method> getSetters(Class<?> cls) {
		List<Method> methods = new ArrayList<>();
		while (cls != null) {
			for (Method method : cls.getDeclaredMethods()) {
				if (method.getName().startsWith("set") && method.getParameterCount() == 1)
					methods.add(method);
			}
			cls = cls.getSuperclass();
		}
		return methods;
	}
	
	private Method searchMethod(String methodName, Class<?> cls) {
		while (cls != null) {
			for (Method method : cls.getDeclaredMethods()) {
				if (method.getName().equals(methodName))
					return method;
			}
			cls = cls.getSuperclass();
		}
		return null;
	}

	private Individual createIndividual(Object obj, OntModel model)
			throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		OWLClass classAnno = getClassAnnotation(obj.getClass());
		OntClass owlClass = base.getOntClass(base.expandPrefix(classAnno.value()));
		Individual owlIndividual = model.createIndividual(uri(obj.getClass()), owlClass);
		owlIndividual.addProperty(RDF.type, owlClass);
		List<Method> getters = getGetters(obj.getClass());
		for (Method getter : getters) {
			if (isIgnored(getter))
				continue;
			OWLProperty propertyAnno = getPropertyAnnotation(getter);
			if (propertyAnno == null)
				continue;
			OntProperty owlProperty = base.getOntProperty(base.expandPrefix(propertyAnno.value()));
			Object value = getter.invoke(obj);
			if (value != null)
				setValue(model, owlIndividual, owlProperty, value);
		}
		return owlIndividual;
	}

	private OWLClass getClassAnnotation(Class<?> cls) {
		OWLClass classAnno = cls.getAnnotation(OWLClass.class);
		if (classAnno != null)
			return cls.getAnnotation(OWLClass.class);
		return mappings.get(cls).getAnnotation(OWLClass.class);
	}

	private boolean isIgnored(Method method) {
		if (method.getDeclaringClass() == Object.class)
			return true;
		Class<?> type = method.getReturnType();
		return !type.isPrimitive() && Modifier.isAbstract(type.getModifiers());
	}
	
	private OWLProperty getPropertyAnnotation(Method method) {
		OWLProperty propertyAnno = method.getAnnotation(OWLProperty.class);
		if (propertyAnno != null)
			return propertyAnno;
		Class<?> mixin = mappings.get(method.getDeclaringClass());
		if (mixin != null) {
			Method aMethod = searchMethod(method.getName(), mixin);
			return aMethod != null ? aMethod.getAnnotation(OWLProperty.class) : null;
		}
		return null;
	}
	
	private String uri(Class<?> cls) {
		OWLIdentifiable identifiableAnno = getIdentifiableAnnotation(cls);
		if (identifiableAnno == null)
			return null;
		String namespace = base.getNsPrefixURI("");
		String uuid = UUID.randomUUID().toString();
		return namespace + uuid;
	}
	
	private OWLIdentifiable getIdentifiableAnnotation(Class<?> cls) {
		OWLIdentifiable identifiableAnno = cls.getAnnotation(OWLIdentifiable.class);
		if (identifiableAnno != null)
			return cls.getAnnotation(OWLIdentifiable.class);
		Class<?> mixin = mappings.get(cls);
		return mixin != null ? mixin.getAnnotation(OWLIdentifiable.class) : null;
	}
	
	private void setValue(OntModel model, Individual owlIndividual, OntProperty owlProperty, Object value)
			throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		if (String.class.isAssignableFrom(value.getClass()))
			owlIndividual.addProperty(owlProperty, String.class.cast(value));
		else if (Integer.class.isAssignableFrom(value.getClass()))
			owlIndividual.addLiteral(owlProperty, Integer.class.cast(value));
		else if (Boolean.class.isAssignableFrom(value.getClass()))
			owlIndividual.addLiteral(owlProperty, Boolean.class.cast(value));
		else if (Double.class.isAssignableFrom(value.getClass()))
			owlIndividual.addLiteral(owlProperty, Double.class.cast(value));
		else if (URI.class.isAssignableFrom(value.getClass()))
			owlIndividual.addLiteral(owlProperty, URI.class.cast(value).toString());
		else 
			owlIndividual.addProperty(owlProperty, createIndividual(value, model));
	}
	
	private Statement getStatementWithProperty(Property prop, List<Statement> statements) {
		for (Statement stmt : statements) {
			if (stmt.getPredicate().equals(prop))
				return stmt;
		}
		return null;
	}
	
	private Object castValue(RDFNode node, Class<?> type) throws InstantiationException, IllegalAccessException, NoSuchMethodException, SecurityException, IllegalArgumentException, InvocationTargetException {
		if (node instanceof Literal) {
			Literal aLiteral = node.asLiteral();
			if (String.class.isAssignableFrom(type))
				return aLiteral.getString();
			else if (Double.class.isAssignableFrom(type) || type.equals(Double.TYPE))
				return aLiteral.getDouble();
			else if (Integer.class.isAssignableFrom(type) || type.equals(Integer.TYPE))
				return aLiteral.getInt();
			else if (Boolean.class.isAssignableFrom(type) || type.equals(Boolean.TYPE))
				return aLiteral.getBoolean();
			else
				return null;
		} else {
			Resource res = node.asResource();
			List<Statement> statements2 = Lists.newArrayList(res.listProperties());
			return readValue(statements2, type);
		}
	}
}
