package br.unisinos.uhospital.owlmapper;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import br.unisinos.uhospital.owlmapper.OWLMapperTest.ObjectIDMixin;
import br.unisinos.uhospital.owlmapper.annotations.OWLClass;
import br.unisinos.uhospital.owlmapper.annotations.OWLProperty;

public class App {

	public static void main(String[] args) throws Exception {
		Class<?> clazz = TerminologyIDMixin.class;
		List<Method> methods = Arrays.asList(clazz.getMethods());
		Optional<Method> method = methods.stream()
				.filter(m -> m.getName().equals("constructor"))
				.findFirst();
		method.ifPresent(m -> System.out.println(m.getDeclaringClass()));
	}
	
//	public abstract class Vehicle {
//		public abstract Number getTankCapacity();
//	}
	
//	public class Truck extends Vehicle {
//		public Integer getTankCapacity() {
//			return 3;
//		}
//	}

	public abstract class ObjectIDMixin {
		public abstract void constructor(
				@OWLProperty("dtrm:value") String value
		);
		
		@OWLProperty("sprm:value")
		public abstract String getValue();

		@OWLProperty("sprm:value")
		public abstract void setValue(String value);
	}

	@OWLClass("sprm:TERMINOLOGY_ID")
	public abstract class TerminologyIDMixin extends ObjectIDMixin {
		public abstract void constructor(
				@OWLProperty("dtrm:value") String value
		);
	}
}