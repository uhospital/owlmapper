package br.unisinos.uhospital.owlmapper;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.apache.jena.ext.com.google.common.collect.Lists;
import org.apache.jena.ontology.Individual;
import org.apache.jena.ontology.OntDocumentManager;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntModelSpec;
import org.apache.jena.ontology.Ontology;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.util.iterator.ExtendedIterator;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openehr.rm.common.archetyped.Locatable;
import org.openehr.rm.datastructure.itemstructure.representation.Element;
import org.openehr.rm.datatypes.basic.DataValue;
import org.openehr.rm.datatypes.basic.DvBoolean;
import org.openehr.rm.datatypes.quantity.DvQuantity;
import org.openehr.rm.datatypes.quantity.datetime.DvDateTime;
import org.openehr.rm.datatypes.quantity.datetime.DvDuration;
import org.openehr.rm.datatypes.quantity.datetime.DvTemporal;
import org.openehr.rm.datatypes.text.CodePhrase;
import org.openehr.rm.datatypes.text.DvCodedText;
import org.openehr.rm.datatypes.text.DvText;
import org.openehr.rm.support.identification.ObjectID;
import org.openehr.rm.support.identification.TerminologyID;

import br.unisinos.uhospital.owlmapper.annotations.OWLClass;
import br.unisinos.uhospital.owlmapper.annotations.OWLProperty;

public class OWLMapperTest {	
	public static OntDocumentManager docMgr;
	public static OntModelSpec modelSpec;

	@BeforeClass
	public static void setUpClass() {
		docMgr = OntDocumentManager.getInstance();
		docMgr.addAltEntry("http://uhospital.unisinos.br/openehr/data-types.owl", "ontologies/data-types.owl");
		docMgr.addAltEntry("http://uhospital.unisinos.br/openehr/support.owl", "ontologies/support.owl");
		docMgr.addAltEntry("http://uhospital.unisinos.br/openehr/data-structures.owl", "ontologies/data-structures.owl");
		docMgr.addAltEntry("http://uhospital.unisinos.br/openehr/common.owl", "ontologies/common.owl");
		
		modelSpec = new OntModelSpec(OntModelSpec.OWL_MEM);
		modelSpec.setDocumentManager(docMgr);
	}

	@Test
	public void testWriteDataValues() throws Exception {
		OntModel model = ModelFactory.createOntologyModel(modelSpec);
		model.setNsPrefix("", "http://example.com/ontology#");
		model.setNsPrefix("dtrm", "http://uhospital.unisinos.br/openehr/data-types.owl#");
		model.setNsPrefix("sprm", "http://uhospital.unisinos.br/openehr/support.owl#");
		
		Ontology ont = model.createOntology("http://example.com/ontology");
		ont.addImport(model.createResource("http://uhospital.unisinos.br/openehr/data-types.owl"));
		ont.addImport(model.createResource("http://uhospital.unisinos.br/openehr/support.owl"));
		model.loadImports();

		OWLMapper mapper = new OWLMapper(model);
		mapper.addMapping(DvQuantity.class, DvQuantityMixin.class);
		mapper.addMapping(DvText.class, DvTextMixin.class);
		mapper.addMapping(DvDuration.class, DvDurationMixin.class);
		mapper.addMapping(DvCodedText.class, DvCodedTextMixin.class);
		mapper.addMapping(DvBoolean.class, DvBooleanMixin.class);
		mapper.addMapping(CodePhrase.class, CodePhraseMixin.class);
		mapper.addMapping(DvTemporal.class, DvTemporalMixin.class);
		mapper.addMapping(DvDateTime.class, DvDateTimeMixin.class);
		mapper.addMapping(ObjectID.class, ObjectIDMixin.class);
		mapper.addMapping(TerminologyID.class, TerminologyIDMixin.class);
		
		OntModel expected = ModelFactory.createOntologyModel(modelSpec);
		expected.read("data_values.owl");
		
		DvQuantity aQuantity = new DvQuantity("mm[Hg]", 120, 0);
		model.add(mapper.writeValue(aQuantity));
		
		DvText aText = new DvText("text");
		model.add(mapper.writeValue(aText));
		
		DvDuration aDuration = new DvDuration("PT24H");
		model.add(mapper.writeValue(aDuration));
		
		CodePhrase aCodePhrase = new CodePhrase("SNOMED-CT", "75367002");
		model.add(mapper.writeValue(aCodePhrase));
		
		DvCodedText aCodedText = new DvCodedText("Blood Pressure", aCodePhrase);
		model.add(mapper.writeValue(aCodedText));
		
		DvBoolean aBoolean = new DvBoolean(true);
		model.add(mapper.writeValue(aBoolean));
		
		DvDateTime aDateTime = new DvDateTime("2016-08-18T12:34:56Z");
		model.add(mapper.writeValue(aDateTime));
				
		Assert.assertTrue(model.isIsomorphicWith(expected));
	}

	@Test
	public void testReadDataValues() throws InstantiationException, IllegalAccessException, 
		NoSuchMethodException, SecurityException, IllegalArgumentException,
		InvocationTargetException, IntrospectionException {
		OntModel model = ModelFactory.createOntologyModel(modelSpec);
		model.setNsPrefix("dtrm", "http://uhospital.unisinos.br/openehr/data-types.owl#");
		model.setNsPrefix("sprm", "http://uhospital.unisinos.br/openehr/support.owl#");
		model.read("data_values.owl");

		OWLMapper mapper = new OWLMapper(model);
		mapper.addMapping(DvQuantity.class, DvQuantityMixin.class);
		mapper.addMapping(DvText.class, DvTextMixin.class);
		mapper.addMapping(DvDuration.class, DvDurationMixin.class);
		mapper.addMapping(DvCodedText.class, DvCodedTextMixin.class);
		mapper.addMapping(DvBoolean.class, DvBooleanMixin.class);
		mapper.addMapping(CodePhrase.class, CodePhraseMixin.class);
		mapper.addMapping(DvTemporal.class, DvTemporalMixin.class);
		mapper.addMapping(DvDateTime.class, DvDateTimeMixin.class);
		mapper.addMapping(ObjectID.class, ObjectIDMixin.class);
		mapper.addMapping(TerminologyID.class, TerminologyIDMixin.class);
		
		ExtendedIterator<Individual> it = model.listIndividuals(resource("dtrm:DV_TEXT", model));
		Individual aIndividual = it.next();
		List<Statement> statements = Lists.newArrayList(aIndividual.listProperties());
		DvText text = (DvText) mapper.readValue(statements, DvText.class);
		DvText expected = new DvText("text");
		Assert.assertEquals(expected, text); 
		
		ExtendedIterator<Individual> it2 = model.listIndividuals(resource("dtrm:DV_QUANTITY", model));
		Individual aIndividual2 = it2.next();
		List<Statement> statements2 = Lists.newArrayList(aIndividual2.listProperties());
		DvQuantity quantity = (DvQuantity) mapper.readValue(statements2, DvQuantity.class);
		DvQuantity expected2 = new DvQuantity("mm[Hg]", 120, 0);
		Assert.assertEquals(expected2, quantity);
		
		ExtendedIterator<Individual> it3 = model.listIndividuals(resource("dtrm:DV_CODED_TEXT", model));
		Individual aIndividual3 = it3.next();
		List<Statement> statements3 = Lists.newArrayList(aIndividual3.listProperties());
		DvCodedText codedText = (DvCodedText) mapper.readValue(statements3, DvCodedText.class);
		CodePhrase codePhrase = new CodePhrase("SNOMED-CT", "75367002");
		DvCodedText expected3 = new DvCodedText("Blood Pressure", codePhrase);
		Assert.assertEquals(expected3, codedText);
		
		ExtendedIterator<Individual> it4 = model.listIndividuals(resource("dtrm:DV_BOOLEAN", model));
		Individual aIndividual4 = it4.next();
		List<Statement> statements4 = Lists.newArrayList(aIndividual4.listProperties());
		DvBoolean aBoolean = (DvBoolean) mapper.readValue(statements4, DvBoolean.class);
		DvBoolean expected4 = new DvBoolean(true);
		Assert.assertEquals(expected4, aBoolean);
		
		ExtendedIterator<Individual> it5 = model.listIndividuals(resource("dtrm:DV_DATE_TIME", model));
		Individual aIndividual5 = it5.next();
		List<Statement> statements5 = Lists.newArrayList(aIndividual5.listProperties());
		DvDateTime aDateTime = (DvDateTime) mapper.readValue(statements5, DvDateTime.class);
		DvDateTime expected5 = new DvDateTime("2016-08-18T12:34:56Z");
		Assert.assertEquals(expected5, aDateTime);
	}

	@Test
	public void testWriteItem() throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		OntModel model = ModelFactory.createOntologyModel(modelSpec);
		model.setNsPrefix("", "http://example.com/ontology#");
		model.setNsPrefix("dtrm", "http://uhospital.unisinos.br/openehr/data-types.owl#");
		model.setNsPrefix("dsrm", "http://uhospital.unisinos.br/openehr/data-structures.owl#");
		model.setNsPrefix("crm", "http://uhospital.unisinos.br/openehr/common.owl#");
		
		Ontology ont = model.createOntology("http://example.com/ontology");
		ont.addImport(model.createResource("http://uhospital.unisinos.br/openehr/data-types.owl"));
		ont.addImport(model.createResource("http://uhospital.unisinos.br/openehr/data-structures.owl"));
		ont.addImport(model.createResource("http://uhospital.unisinos.br/openehr/common.owl"));
		model.loadImports();

		OWLMapper mapper = new OWLMapper(model);
		mapper.addMapping(DvText.class, DvTextMixin.class);
		mapper.addMapping(Locatable.class, LocatableMixin.class);
		mapper.addMapping(Element.class, ElementMixin.class);
		
//		OntModel expected = ModelFactory.createOntologyModel(modelSpec);
//		expected.read("item.owl");
		
		Element aElement = new Element("at0001", "element", new DvText("text"));
		model.add(mapper.writeValue(aElement));
		
		model.write(System.out, "N3");

//		Assert.assertTrue(model.isIsomorphicWith(expected));
	}
	
	private Resource resource(String prefixed, OntModel model) {
		return model.getResource(model.expandPrefix(prefixed));
	}

	public abstract class DataValueMixin {
		@OWLProperty("dtrm:value")
		public abstract String getValue();

		@OWLProperty("dtrm:value")
		public abstract void setValue(String value);
	}

	@OWLClass("dtrm:DV_QUANTITY")
	public abstract class DvQuantityMixin extends DataValueMixin {
		@OWLProperty("dtrm:magnitude")
		public abstract double getMagnitude();
		
		@OWLProperty("dtrm:magnitude")
		public abstract void setMagnitude(double magnitude);
		
		@OWLProperty("dtrm:units")
		public abstract String getUnits();
		
		@OWLProperty("dtrm:units")
		public abstract void setUnits(String units);
		
		@OWLProperty("dtrm:precision")
		public abstract int getPrecision();
		
		@OWLProperty("dtrm:precision")
		public abstract void setPrecision(int precision);
	}

	@OWLClass("dtrm:DV_TEXT")
	public abstract class DvTextMixin extends DataValueMixin {
	}

	@OWLClass("dtrm:DV_BOOLEAN")
	public abstract class DvBooleanMixin extends DataValueMixin {
	}

	@OWLClass("dtrm:DV_DATE_TIME")
	public abstract class DvDateTimeMixin {
		public abstract void constructor(
				@OWLProperty("dtrm:value") String value
		);
	}

	@OWLClass("dtrm:DV_TEMPORAL")
	public abstract class DvTemporalMixin {
		@OWLProperty("dtrm:value")
		public abstract String getValue();

		@OWLProperty("dtrm:value")
		public abstract void setValue(String value);
	}

	@OWLClass("dtrm:DV_CODED_TEXT")
	public abstract class DvCodedTextMixin extends DataValueMixin {
		@OWLProperty("dtrm:defining_code")
		public abstract CodePhrase getDefiningCode();
		
		@OWLProperty("dtrm:defining_code")
		public abstract void setDefiningCode(CodePhrase definingCode);
	}

	@OWLClass("dtrm:DV_DURATION")
	public abstract class DvDurationMixin extends DataValueMixin {
	}

	public abstract class ObjectIDMixin {
		public abstract void constructor(
				@OWLProperty("sprm:value") String value
		);
		
		@OWLProperty("sprm:value")
		public abstract String getValue();

		@OWLProperty("sprm:value")
		public abstract void setValue(String value);
	}

	@OWLClass("sprm:TERMINOLOGY_ID")
	public abstract class TerminologyIDMixin extends ObjectIDMixin {
	}

	@OWLClass("dtrm:CODE_PHRASE")
	public abstract class CodePhraseMixin {
		@OWLProperty("dtrm:terminology_id")
		public abstract TerminologyID getTerminologyId();
		
		@OWLProperty("dtrm:terminology_id")
		public abstract void setTerminologyId(TerminologyID terminologyId);
		
		@OWLProperty("dtrm:code_string")
		public abstract String getCodeString();
		
		@OWLProperty("dtrm:code_string")
		public abstract void setCodeString(String codeString);
	}
	
	@OWLClass("crm:LOCATABLE")
	public abstract class LocatableMixin {
		@OWLProperty("crm:name")
		public abstract DvText getName();
		
		@OWLProperty("crm:name")
		public abstract void setName(String name);
		
		@OWLProperty("crm:archetype_node_id")
		public abstract String getArchetypeNodeId();
		
		@OWLProperty("crm:archetype_node_id")
		public abstract void setArchetypeNodeId(String nodeId);
	}
	
	@OWLClass("dsrm:ELEMENT")
	public abstract class ElementMixin {
		@OWLProperty("dsrm:null_flavour")
		public abstract void setNullFlavour(DvCodedText codedText);
		
		@OWLProperty("dsrm:null_flavour")
		public abstract void getNullFlavour();
		
		@OWLProperty("dsrm:value")
		public abstract void setValue(DataValue value);
		
		@OWLProperty("dsrm:value")
		public abstract void getValue();
	}
}
