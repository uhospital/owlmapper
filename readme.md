# OWLMapper

This tool maps POJOs (Plain Old Java Object) to OWL Individuals. The mapping between Java Objects and OWL is done using annotations.
